package org.ifp.ejercicio5;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static java.lang.System.out;

public class Ftp {

    private static void listFilesRoot(FTPClient ftpClient) throws IOException {
        out.printf("Mostrando archivos desde la raíz %n");
        Arrays.stream(ftpClient.listFiles()).forEach(out::println);
    }

    private static void checkConnection(String USUARIO, String PASSWORD, boolean loginSatisfactorio, FTPClient ftpClient, int respuesta) throws IOException {
        if (!FTPReply.isPositiveCompletion(respuesta)) System.out.printf("Algo ha salido mal: %s %n", respuesta);
        else loginSatisfactorio = ftpClient.login(USUARIO, PASSWORD);

        if (loginSatisfactorio) System.out.println("Conexión correcta");
    }

    private static void listFilesDownload(FTPClient ftpClient) throws IOException {
        out.printf("Mostrando archivos desde /download %n");
        Arrays.stream(ftpClient.listFiles("/download")).forEach(out::println);
    }

    private static void uploadFile(FTPClient ftpClient) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("archivo.docx");
        boolean uploaded = ftpClient.appendFile("/upload/archivo2.docx", fileInputStream);
        fileInputStream.close();
        if (uploaded) out.printf("Subido con éxito %n");
        else out.printf("No se ha podido subir el fichero %n");
    }

    private static void downloadFile(FTPClient ftpClient) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("archivo.docx");
        boolean downloaded = ftpClient.retrieveFile("/upload/archivo.docx", fileOutputStream);
        fileOutputStream.close();
        if (downloaded) out.printf("Descargado con éxito %n");
        else out.printf("No se ha podido descargar el fichero %n");
    }

    private static void disconnect(FTPClient ftpClient) throws IOException {
        out.print("Desconexión del servidor");
        ftpClient.disconnect();
    }

    public void ftpMethods() throws IOException {
        final String SERVIDOR = "demo.wftpserver.com";
        final int PUERTO = 21;
        final String USUARIO = "demo";
        final String PASSWORD = "demo";
        boolean loginSatisfactorio = false;

        FTPClient ftpClient = new FTPClient();

        ftpClient.connect(SERVIDOR, PUERTO);
        int respuesta = ftpClient.getReplyCode();

        checkConnection(USUARIO, PASSWORD, loginSatisfactorio, ftpClient, respuesta);
        listFilesRoot(ftpClient);
        listFilesDownload(ftpClient);
        downloadFile(ftpClient);
        uploadFile(ftpClient);
        disconnect(ftpClient);
    }
}
