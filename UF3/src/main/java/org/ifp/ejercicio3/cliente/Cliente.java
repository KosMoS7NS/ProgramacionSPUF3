package org.ifp.ejercicio3.cliente;


import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import static java.lang.System.in;

public class Cliente {

    public void socketClient() throws IOException {
        Socket socket = new Socket("localhost", 9999);
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

        String respuesta = inputOperation();
        System.out.println("Operación: " + respuesta);

        dataOutputStream.writeUTF(respuesta);
        dataOutputStream.close();
        socket.close();
    }

    private static String inputOperation() {
        Scanner scanner = new Scanner(in);
        System.out.println("Introduce la operación: ");
        String respuesta = scanner.nextLine();
        return respuesta;
    }
}
