package org.ifp.ejercicio3.servidor;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Servidor {

    private static List<String> filterOperator(String mensaje) {
        List<String> operator = Arrays.stream(new String[]{"+", "-", "*"})
                .filter(s -> s.contains(mensaje.substring(1, 2)))
                .map(s -> mensaje.substring(1, 2))
                .collect(Collectors.toList());
        return operator;
    }

    private static Float operationResults(String mensaje, String[] split) {
        Float number1 = Float.parseFloat(split[0]);
        Float number2 = Float.parseFloat(split[1]);

        Function<Float, Float> suma = n1 -> n1 + number2;
        Function<Float, Float> resta = n1 -> n1 - number2;
        Function<Float, Float> multiplicacion = n1 -> n1 * number2;

        Map<String, Float> map = new HashMap<>();
        map.put("+", suma.apply(number1));
        map.put("-", resta.apply(number1));
        map.put("*", multiplicacion.apply(number1));

        return map.get(mensaje.substring(1, 2));
    }

    public void socketServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(9999);
        Socket socket = serverSocket.accept();

        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        String mensaje = dataInputStream.readUTF();

        System.out.println("El resultado es: " + operationResults(
                mensaje,
                mensaje.split(filterOperator(mensaje).toString())
        ));

        dataInputStream.close();
        socket.close();
    }
}
