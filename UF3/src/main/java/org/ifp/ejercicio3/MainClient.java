package org.ifp.ejercicio3;

import org.ifp.ejercicio3.cliente.Cliente;

import java.io.IOException;

public class MainClient {
    public static void main(String[] args) throws IOException {
        new Cliente().socketClient();
    }
}