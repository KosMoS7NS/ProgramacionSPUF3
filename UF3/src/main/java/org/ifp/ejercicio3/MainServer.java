package org.ifp.ejercicio3;

import org.ifp.ejercicio3.servidor.Servidor;

import java.io.IOException;

public class MainServer {
    public static void main(String[] args) throws IOException {
        new Servidor().socketServer();
    }
}